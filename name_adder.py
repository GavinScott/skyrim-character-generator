import json
import sys

COMMA_FLAG = '-c'
INPUT_FLAG = '-i'

clean = lambda s: ''.join([c for c in s if c.isalpha() or c in ['-', "'"]]).strip()

with open('names.json', 'r') as f:
    data = json.load(f)

race = sys.argv[1]
gender = sys.argv[2]

if '-i' not in sys.argv:
    names = [n for n in sys.argv[3:] if n != COMMA_FLAG]
    if COMMA_FLAG in sys.argv:
        names = [n.strip() for n in ' '.join(names).split(',')]
else:
    names = input('Enter names:')
    names = [n.strip() for n in names.split(',')]

print('\n\nRace: {}, Gender: {}'.format(race, gender))

all_names = set(data[race][gender])
for name in names:
    all_names.add(clean(name))

data[race][gender] = list(all_names)

with open('names.json', 'w') as f:
    f.write(json.dumps(data, indent=4, sort_keys=True))

print()
print(all_names)