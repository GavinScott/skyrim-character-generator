import random
import json

RACES = [
    'Altmer',
    'Argonian',
    'Bosmer',
    'Breton',
    'Dunmer',
    'Imperial',
    'Khajiit',
    'Nord',
    'Orc',
    'Redguard'
]

SKILLS = [
    'Alteration',
    'Conjuration',
    [
        'Destruction - Fire',
        'Destruction - Frost',
        'Destruction - Lightning',
    ],
    'Enchanting',
    'Illusion',
    'Restoration',
    'Block',
    'Heavy Armor',
    [
        'One-Handed (Sword)',
        'One-Handed (Mace)',
        'One-Handed (Axe)',
    ],
    'Smithing',
    [
        'Two-Handed (Greatsword)',
        'Two-Handed (Battle Axe)',
        'Two-Handed (War Hammer)',
    ],
    'Alchemy',
    'Light Armor',
    'Lockpicking',
    'Pickpocket',
    [
        'Sneak',
        'Archery',
    ],
    'Speech'
]

def get_skills():
    random.shuffle(SKILLS)
    chooser = lambda x: x if type(x) == str else random.choice(x)
    return [chooser(s) for s in SKILLS[:3]]

# load names
with open('names.json', 'r') as f:
    names = json.load(f)

# stats
print('\n SKYRIM CHARACTER GENERATOR')
total = 0
print('\n Name Stats:')
for race in RACES:
    num = len(names[race]['Male']) + len(names[race]['Female'])
    print('\t', race, ":", num)
    total += num
print('\n Total: {}\n'.format(total))
print('-'*70, '\n')

# generator
gender = random.choice(['Male', 'Female'])
race = random.choice(RACES)

skills = get_skills()

print(' RACE   :       ', race)
print(' GENDER :       ', gender)
print(' NAME   :       ', random.choice(names[race][gender]))
print('\n SKILLS :        ' + '\n\t         '.join(skills))

print('\n AFFILIATION :  ',
    random.choice(['Imperials', 'Stormcloaks']))
print(' SPECIAL?    :  ',
    random.choice(['Werewolf', 'Vampire', 'Monster Hunter']))
print('\n INTERESTED IN :', random.choice(['Men', 'Women']))

print("\n" + '-'*70, '\n')

print(' APPEARENCE GENERATION:')
while(True):
    try:
        num = input('\n Enter the number of options: ')
        num = int(num)
        print('\tChoose option #' +
            str(random.choice(range(1, num + 1))))
    except ValueError:
        pass
    except:
        print("\n\n" + '-'*70)
        print('\n Have Fun!\n')
        exit()